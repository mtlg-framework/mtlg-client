// Modules to control application life and create native browser window
const {app, shell, dialog, ipcMain, Menu, BrowserWindow, globalShortcut, screen} = require('electron')
const Store = require('electron-store')

const {videoRecorder} = require('./mediaStreamRecorder/videorecorder')
const {sequenceRunner} = require('./sequencing/sequenceRunner')

const {sequences} = require('./sequencing/prebuildSequences')

const {modalGameURL} = require('./modalGameURL/modal')
const {remoteConfigurator} = require('./remoteconfigurator/remoteconfigurator')
const path = require('path')
const fs = require("fs");

// This is important to allow Multi-touch interaction in game, esp. drag interaction
app.commandLine.appendSwitch('touch-events', 'enabled')

// This set takes the currently selected sources for recording
// main is responsible for coordination and menu, so UI selects sources
// and this Set is passed into the respective recorders

let selectedSources = new Set();

let selectedScreens = new Set();

// This part persists the clients configuration
const clientConfigDefaults = {
    tmpRecordingPath: ["tmp/"],
    horizontal: true,
    recordScreen: false,
    recordCams: false,
    recordingSources: [],
    recordingScreens: [],
    remoteConfigServer: "",
    remoteConfigToken: "",
    skipWizard: false,
    gameURL: "",
    defaultGameURL: "https://lernspiele.informatik.rwth-aachen.de",
    defaultOrchestrator: "https://mtlg.elearn.rwth-aachen.de",
    remoteUrl: "",
    mode: 'mode1',
    gamePackage: ""
}
let clientConfig = undefined;
const store = new Store();
if (!(clientConfig = store.get('config'))) {
    clientConfig = clientConfigDefaults;
    store.set('config', clientConfig);
} else {
    selectedSources = new Set(clientConfig.recordingSources);
    selectedScreens = new Set(clientConfig.recordingScreens);
}

function resetClientConfig(){
    clientConfig = clientConfigDefaults;
    store.set('config', clientConfig);
}
global.clientConfig = clientConfig;

let startSequence = false;
let inWizard = false;
let importing = false

function updateClientConfig(key, value) {
    clientConfig[key] = value;
    store.set('config', clientConfig);
    global.clientConfig = clientConfig;
}


// Create Main window for wizard, config, ...
let mainWindow;

function createWindow() {
    // Create the browser window.
    let options = store.get('bounds')||{ width: 800, height: 600}

    options.webPreferences = {
        preload: path.join(__dirname, 'preload.js'),
        nodeIntegration: false,
        contextIsolation: true,
        enableRemoteModule: true
    }
    options.show = false

    mainWindow = new BrowserWindow(options)

    mainWindow.on('close', async function () {
        store.set('bounds', mainWindow.getBounds());

        console.log(await videoRecorder.emergencyStop());

        if (process.platform !== 'darwin') app.quit()
    })
    global.winId = mainWindow.getMediaSourceId()
    mainWindow.once('ready-to-show', mainWindow.show)
    mainWindow.on("moved",setScreenID)
}

async function setScreenID() {
    if(videoRecorder) global.screenId = await videoRecorder.convertDisplayId(screen.getDisplayMatching(mainWindow.getBounds()).id)
}
//setInterval(windowID,5000)

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})

app.whenReady().then(() => {
    createWindow()
    createMenu();

    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
    processConfig()
})

function processConfig(){
    startSequence = false;
    if(!clientConfig.skipWizard){
        inWizard = true
        resetClientConfig()
        //mainWindow.loadFile('sequencing/showmessage.html').then(r => console.log("Loaded Wizard"))
        mainWindow.loadFile('setupwizard/index.html').then(r => console.log("Loaded Wizard"))
    } else startSequence = true
    let seq,cfg
    let remote = false
    switch (clientConfig.mode){//todo introduce more readable constants/enums?
        case '1':
            seq = sequences.mode1Sequence
            cfg = sequences.mode1Config
            break
        case '2':
            seq = sequences.mode1Sequence
            cfg = sequences.mode1Config
            break
        case '3':
        case '4':
        case '5':
            seq = sequences.remoteSequence
            cfg = sequences.remoteConfig
            global.socketURL=clientConfig.remoteUrl||clientConfigDefaults.defaultOrchestrator
            remote = true
            break
        case '6':
            seq = sequences.mode1Sequence
            cfg = sequences.mode1Config
            break
        default:
            seq = sequences.demoSequence
            cfg = sequences.demoConfiguration
    }
    console.log("SocketURL",global.socketURL)
    videoRecorder.init(clientConfig.tmpRecordingPath[0]).then(async () => {
        await videoRecorder.getAvailableStreams('screen').then(prepareScreenList)
        videoRecorder.getAvailableStreams('video').then(prepareVideoDeviceList)
        setScreenID()
    })

    //todo only if remote config is enabled
    //if(clientConfig.remoteConfigServer!=="") remoteConfigurator.init(videoRecorder)
    remoteConfigurator.init(videoRecorder,sequenceRunner,global.socketURL)
    //else {
        let options = {
            win: mainWindow,
            videoRecorder,
            clientConfig
        }
        if (startSequence) {
            if(importing) setTimeout(processConfig,50) //if packageimport is still going on in the background, wait a little more
            else {
                sequenceRunner.init(cfg, options)
                sequenceRunner.startSequence(seq)
            }

        }
    //}
}

app.on('will-quit', async () => {
    // Unregister all shortcuts.
    globalShortcut.unregisterAll()
})

function importGamePackage(pathToPackage,nameOfGame = undefined){
    if(nameOfGame===undefined) nameOfGame=path.basename(pathToPackage,'.mtlg');
    return new Promise(((resolve, reject) => {
        require("targz").decompress({
            src: pathToPackage,
            dest: path.join(__dirname,'games',nameOfGame)
        }, function(err){
            if(err) {
                console.log(err);
                reject()
            } else {
                console.log("Done!");
                resolve(path.join(__dirname,'games',nameOfGame,'index.html'))
            }
        });
    }))
}

function prepareScreenList(devices) {
    let deviceMenu = [];
    let deviceStreamMenu = [];
    console.log("executed screenlistpreparation")
    devices.forEach(device => {
        console.log(device)
        let menuitem = {
            label: device.name,
            id: device.id,
            type: 'checkbox',
            checked: selectedSources.has(device.id),
            click(item, win, evt) {
                if (item.checked) selectedScreens.add(item.id)
                else selectedScreens.delete(item.id)
                updateClientConfig('recordingScreens', [...selectedScreens])
            }
        }
        let streamItem = {
            label: device.name,
            id: device.id,
            click(item, win, evt) {
                videoRecorder.prepareSource(device.id, device.type).then(() => {
                    videoRecorder.startStreaming(item.id)
                })
            }
        }
        deviceStreamMenu.push(streamItem)
        deviceMenu.push(menuitem)
    })
    menuTemplate.screenRecorder.submenu[2].submenu = deviceMenu
    menuTemplate.screenRecorder.submenu[3].submenu = deviceStreamMenu
    createMenu()
}

function prepareVideoDeviceList(devices) {
    let devicemenu = [];
    let deviceStreamMenu = [];
    console.log("executed videolist preparation")
    devices.forEach(device => {

        let menuitem = {
            label: device.name,
            id: device.id,
            type: 'checkbox',
            checked: selectedSources.has(device.id),
            click(item, win, evt) {
                if (item.checked) selectedSources.add(item.id)
                else selectedSources.delete(item.id)
                updateClientConfig('recordingSources', [...selectedSources])
            }
        }
        let streamItem = {
            label: device.name,
            id: device.id,
            click(item, win, evt) {
                videoRecorder.prepareSource(device.id, device.type,"manual").then(() => {
                    videoRecorder.startStreaming(item.id)
                })
            }
        }
        deviceStreamMenu.push(streamItem)
        devicemenu.push(menuitem)

    })

    menuTemplate.videoRecorder.submenu[2].submenu = devicemenu
    menuTemplate.videoRecorder.submenu[3].submenu = deviceStreamMenu
    createMenu()
}

// Possibly deprecated
ipcMain.on('fromGameURL', function (event, args) {
    switch (args.msg) {
        case "setURL":
            updateClientConfig("tmpGameURL", args.url);
            break;
    }
});

ipcMain.on('toClient', async function (event, args) {
    switch (args.cmd) {
        case "setConfig":
            if(inWizard){
                console.log("Arguments given:",args)
                switch(args.id){
                    case 'gamePackage':
                        importing = true;
                        let pathOfPackage = await importGamePackage(args.value)
                        importing = false
                        updateClientConfig('gamePackage',pathOfPackage)
                        break
                    case 'wizardCompleted':
                        updateClientConfig('skipWizard',true)
                        processConfig()
                        break;
                    case 'lock':
                        if(args.value) lock();
                        break;
                    default:
                        updateClientConfig(args.id, args.value)
                }
            } else {
                console.log("Some content out of wizard tried to modify config")
            }
            //updateClientConfig("tmpGameURL", args.url);
            break;
    }
});



// Generate the actual menu from the template (also called after updates on the menu)
function createMenu() {
    let menu
    if(!fs.existsSync("./.lock")) {
        menu = Menu.buildFromTemplate([
            menuTemplate.settings,
            menuTemplate.screenRecorder,
            menuTemplate.videoRecorder,
            menuTemplate.info
        ])
    }
    else menu = Menu.buildFromTemplate(menuTemplateLocked)
    Menu.setApplicationMenu(menu);
}

function lock(){
    fs.writeFile('./.lock', 'Delete this to unlock all menu options', function (err) {
        if (err) return console.log(err);
        console.log('Client is locked on next restart');
        createMenu()
    });
}

// Create a template for the menu
let menuTemplate = {};
menuTemplate.settings = {
    label: 'Einstellungen',
    submenu: [
        {
            label: 'Einrichtungsassistenten starten',
            click() {
                sequenceRunner.interruptSequence()
                updateClientConfig('skipWizard',false)
                processConfig()
            }
        },
        {
            label: 'Select Game',
            submenu: [
                {
                    label: 'Use URL',
                    type: 'checkbox',
                    click(menuItem) {
                        updateClientConfig("useURL", menuItem.checked);
                    },
                    //checked: clientConfig.useURL
                },
                {
                    label: 'Spieldatei auswählen',
                    click() {
                        let rpath = dialog.showOpenDialogSync({properties: ['openFile']});
                        if (rpath) updateClientConfig("tmpGamePath", rpath);
                    }
                },
                {
                    label: 'URL zum Spiel eingeben',
                    click() {
                        modalGameURL.init(clientConfig.tmpGameURL);
                    }
                },
            ]
        },
        {
            label: 'Aufzeichnungsverzeichnis auswählen',
            click() {
                let rpath = dialog.showOpenDialogSync({properties: ['openDirectory']});
                if (rpath) {
                    fs.access(rpath[0], fs.constants.W_OK, function (err) {
                        if (err) {
                            console.error("can't write to given path");
                        } else {
                            videoRecorder.setRecordingPath(rpath[0]);
                            updateClientConfig('tmpRecordingPath', rpath);
                        }
                    });

                }
            }
        },
        {
            label: 'About MTLG',
            click() {
                shell.openExternal('https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/home').then(r => console.log("Opened documentation"))
            }
        },
        {
            label: 'Exit',
            click() {
                app.quit()
            }
        }]
}

menuTemplate.screenRecorder = {
    label: 'Screen-Recorder',
    submenu: [
        {
            label: 'Aufzeichnung starten',
            click() {
                videoRecorder.setPrefix("Test")
                Promise.all(Array.from(selectedScreens).map(s => videoRecorder.prepareSource(s, 'screen'))).then(() => videoRecorder.startRecording(Array.from(selectedScreens)))
            }
        },
        {
            label: 'Aufzeichnung stoppen',
            click() {
                videoRecorder.stopRecording(Array.from(selectedScreens)).then((files) => console.log("done", files))
            }
        },
        {
            label: 'Aufzeichnungsgeräte',
            id: 'videoRecordingDevices'
        },
        {
            label: 'Vorschaustream starten',
            id: 'videoStreamingDevices'
        },
        {
            label: 'Hauptfenster streamen',
            click() {
                videoRecorder.prepareSource(mainWindow.getMediaSourceId(), 'window').then(()=>videoRecorder.startStreaming(mainWindow.getMediaSourceId()))
            }
        }
    ]
}

menuTemplate.videoRecorder = {
    label: 'Videorecorder',
    submenu: [
        {
            label: 'Aufzeichnung starten',
            click() {
                Promise.all(Array.from(selectedSources).map(s => videoRecorder.prepareSource(s, 'video',"manual"))).then(() => videoRecorder.startRecording(Array.from(selectedSources)))
            }
        },
        {
            label: 'Aufzeichnung stoppen',
            click() {
                videoRecorder.stopRecording(Array.from(selectedSources)).then((files) => console.log("done", files))
            }
        },
        {
            label: 'Aufzeichnungsgeräte',
            id: 'videoRecordingDevices'
        },
        {
            label: 'Vorschau-Stream starten',
            id: 'videoStreamingDevices'
        },
        {
            label: 'Vorschau-Stream beenden',
            click() {
                videoRecorder.stopStreaming()
            }
        }
    ]
}

menuTemplate.info = {
    label: 'Info',
    click(item, win) {
        win.webContents.toggleDevTools()
    }
}

const menuTemplateLocked = [{
    label: 'Info',
    submenu: [
        {
            label: 'About MTLG',
            click() {
                shell.openExternal('https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/home').then(r => console.log("Opened documentation"))
            }
        },
        {
            label: 'Exit',
            click() {
                app.quit()
            }
        }]
}]
