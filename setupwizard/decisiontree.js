let usage = {
    left: {
        icon: "fa-child",
        title: "Ich will nur spielen",
        text: "Zum Einsatz dem persönlichen Endgerät",
        next: (() => settingsForm(mode_justplay))
    },
    right: {
        icon: "fa-id-card",
        title: "Ich habe andere Pläne",
        text: "Einsatz in der Schule oder im Lernlabor",
        next: (() => setThisOrThat(usage_prof))
    }
}

let usage_prof = {
    left: {
        icon: "fa-chalkboard-teacher",
        title: "Ich setze dieses Gerät im Unterricht ein",
        text: "Im Schuleinsatz kann die Lehrkraft Vorgaben machen",
        next: (() => settingsForm(mode_inSchool))
    },
    right: {
        icon: "fa-user-graduate",
        title: "Mein Interesse ist wissenschaftlich",
        text: "MTLG ist als Forschungsinstrument entwickelt worden",
        next: (() => setThisOrThat(usage_prof_science))
    },
    back: (() => setThisOrThat(usage))
}

let usage_prof_science = {
    left: {
        icon: "fa-people-carry",
        title: "An diesem Gerät arbeiten Probanden kollaborativ",
        text: "Es wird hier auf einem Multi-Touch-Tabletop betrieben",
        next: (() => settingsForm(mode_mttt))
    },
    right: {
        icon: "fa-male",
        title: "Dieses Gerät nutzt ein einzelner Proband",
        text: "Supplementär im Lernlabor oder als Individualstudie",
        next: (() => setThisOrThat(usage_prof_science_single))
    },
    back: (() => setThisOrThat(usage_prof))
}

let usage_prof_science_single = {
    left: {
        icon: "fa-puzzle-piece",
        title: "Dieses Gerät generiert Meta-Daten im Laborsetup",
        text: "Fragebögen, Vortests und weitere Online-Erhebungen",
        next: (() => settingsForm(mode_supplementingClient))
    },
    right: {
        icon: "fa-image",
        title: "Die gesamte Studie läuft auf diesem Gerät ab",
        text: "Individualforschung auf dem Gerät",
        next: (() => setThisOrThat(usage_prof_science_single_dedicated))
    },
    back: (() => setThisOrThat(usage_prof_science))
}

let usage_prof_science_single_dedicated = {
    left: {
        icon: "fa-globe",
        title: "Die Studie läuft online",
        text: "Lokale Konfiguration, Datensammlung und Lernspiel laufen online",
        next: (() => settingsForm(mode_standalone_online))
    },
    right: {
        icon: "fa-map-marker",
        title: "Die Studie läuft offline",
        text: "Import des Spiels, lokale Konfiguration der Parameter",
        next:(()=>settingsForm(mode_standalone_offline))
    },
    back: (() => setThisOrThat(usage_prof_science_single))
}

