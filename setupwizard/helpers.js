
function setThisOrThat(decision) {
    document.getElementById('decisiontree').hidden = false;
    document.getElementById('inputform').hidden = true;
    document.getElementById('welcome_text').innerText = "Dieser Einrichtungsassistent wird dir helfen, den Client so einzurichten, wie du ihn einsetzen möchtest!\n" +
        "    Der Client startet in Zukunft mit den gewählten Einstellungen. Der Assistent kann jederzeit im Menü neu gestartet werden.";
    changeIcon("iconleft", decision.left.icon);
    changeIcon("iconright", decision.right.icon);
    changeText("titleleft", decision.left.title);
    changeText("titleright", decision.right.title);
    changeText("textleft", decision.left.text);
    changeText("textright", decision.right.text);
    changeOnClick("cardleft", decision.left.next);
    changeOnClick("cardright", decision.right.next);
    if(decision.back) document.getElementById('backButton').style.visibility = "visible"
    else document.getElementById('backButton').style.visibility = "hidden"
    document.getElementById('backButton').onclick = (() => decision.back())
}

function settingsForm(setting) {
    document.getElementById('decisiontree').hidden = true;
    document.getElementById('welcome_text').innerText = setting.description;

    document.getElementById('inputform').hidden = false;
    let htmlcontent = "";
    setting.inputfields.forEach((s) => {
        htmlcontent += '<div class="field">'
        switch (s.type) {
            case "label":
                htmlcontent += "<label>" + s.name + "</label>";
                break;
            case "options":
                htmlcontent += "<label class='label'>" + s.name + "</label><select id='" + s.id + "'>"
                for (const [key, value] of Object.entries(s.value)) {
                    htmlcontent += "<option value=" + key + ">" + value + "</option>";
                }
                htmlcontent += "</select>";
                break;
            case "checkbox":
                htmlcontent += "<label class='checkbox'><input type='checkbox' id='" + s.id + "'> " + s.name + "</label>"
                break;
            case "text":
                htmlcontent += "<label class=\"label\">" + s.name + "</label><div class=\"control\"><input class='input' type='text' placeholder='" + s.placeholder + "' id='" + s.id + "'>"
                break;
            case "file":
                htmlcontent += "<div class=\"file\">\n" +
                    "  <label class=\"file-label\">\n" +
                    "    <input class=\"file-input\" type=\"file\" name=\"resume\" accept='.mtlg' id='" + s.id + "'>\n" +
                    "    <span class=\"file-cta\">\n" +
                    "      <span class=\"file-icon\">\n" +
                    "        <i class=\"fas fa-upload\"></i>\n" +
                    "      </span>\n" +
                    "      <span class=\"file-label\">\n" +
                    s.name +
                    "      </span>\n" +
                    "    </span>\n" +
                    "  </label>\n" +
                    "</div>";
                break;
            case "hidden":
                htmlcontent += "<input type='hidden' id='" + s.id + "' value='" + s.value + "' />";
                break;
        }
        htmlcontent += '</div>'
    })
    htmlcontent += '<div class="has-text-centered"><button class="button" id="submitButton">Los geht\'s</button></div> '
    document.getElementById('formcontent').innerHTML = htmlcontent;
    document.getElementById('submitButton').onclick = (() => setting.next(setting))
    document.getElementById('backButton').style.visibility = "visible"
    document.getElementById('backButton').onclick = (() => setting.back())
}

function evaluateForm(setting) {
    let interrupt = false;
    console.log("interrupted state",interrupt)
    setting.inputfields.forEach((s) => {
        let value
        switch (s.type) {
            case "checkbox":
                value = document.getElementById(s.id).checked;
                break;
            case "text":
            case "hidden":
            case "options":
                value = document.getElementById(s.id).value;
                break;
            case "file":
                value = (document.getElementById(s.id).files.length>0)?document.getElementById(s.id).files[0].path:undefined;
                break;
        }
        if((value===undefined||value==="")&&s.mandatory){
            console.log("interrupted by",s);
            interrupt=true;

        }
        if(value!==undefined) mtlgClientBridge.send({'cmd': 'setConfig', 'id': s.id, value})
    })
    //todo react to interrupt = true, or react to input at all
    if(interrupt)console.log("An interrupt was triggered",setting)
    else mtlgClientBridge.send({'cmd': 'setConfig', 'id': 'wizardCompleted', 'value':true})
}

function changeIcon(selector, icon) {
    let elem = document.getElementById(selector);
    /*    elem.classList.forEach((name) => {
                if (name.startsWith('fa-')) {
                    elem.classList.remove(name);
                }
            }
        );*/
    elem.classList = null;
    elem.classList.add("fas");
    elem.classList.add(icon);
    elem.classList.add("fa-10x");
}

function changeText(selector, newText) {
    document.getElementById(selector).innerText = newText
}

function changeOnClick(selector, action) {
    document.getElementById(selector).onclick = action;
}
