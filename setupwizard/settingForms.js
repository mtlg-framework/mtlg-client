//done
let mode_justplay = {
    description: "Der gewählte Modus lässt Dich einfach spielen. Langfristig wirst du hier noch entscheiden können, ob und welche Daten erfasst werden. Bis dahin hängt es vom gewählten Spiel ab, ob anonyme Nutzungsstatistiken übertragen werden.",
    inputfields: [
        {
            name: "Im Vollbild-Modus spielen. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Internetadresse",
            type: "text",
            id: "gameURL",
            placeholder: "https://lernspiele.informatik.rwth-aachen.de",
            tooltip: "Die Adresse, von der das Spiel geladen wird"
        },
        {
            name: "oder",
            type: "label"
        },
        {
            name: "Spieldatei",
            type: "file",
            id: "gamePackage",
            value: ".mtlg",
            tooltip: "Wenn Du eine Spieldatei hast, kannst du auch diese laden"
        },
        {
            name: "Sind beide gegeben hat die Spieldatei Vorrang. Wird nichts angegeben, wird der RWTH/Learntech-Spieleserver verwendet.",
            type: "label"
        },
        {
            id: "mode",
            value: 1,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage)
}
//done
let mode_inSchool = {
    description: "Der gewählte Modus startet aktuell nur das gewählte Lernspiel. Langfristig wird hier auch die Möglichkeit zur Steuerung vom PC der Lehrkraft vorgesehen werden.",
    inputfields: [
        {
            name: "Im Vollbild-Modus spielen. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Spiel sperren. Wenn aktiviert startet das gewählte Spiel automatisch. Das Menü wird eingeschränkt und dieser Assistent ist nicht mehr zugänglich, bis die Datei .lock im Clientverzeichnis gelöscht wird.",
            type: "checkbox",
            id: "lock",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Internetadresse",
            type: "text",
            id: "gameURL",
            placeholder: "https://lernspiele.informatik.rwth-aachen.de",
            tooltip: "Die Adresse, von der das Spiel geladen wird"
        },
        {
            name: "oder",
            type: "label"
        },
        {
            name: "Spieldatei",
            type: "file",
            id: "gamePackage",
            value: ".mtlg",
            tooltip: "Wenn Du eine Spieldatei hast, kannst du auch diese laden"
        },
        {
            name: "Sind beide gegeben hat die Spieldatei Vorrang. Wird nichts angegeben, wird der RWTH/Learntech-Spieleserver verwendet.",
            type: "label"
        },
        {
            id: "mode",
            value: 2,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage_prof)
}
//done
let mode_mttt = {
    description: "Der gewählte Modus startet aktuell das gewählte Lernspiel. Option zur Remote-Steuerung kommt.",
    inputfields: [
        {
            name: "Im Vollbild-Modus spielen. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Spiel sperren. Wenn aktiviert startet das gewählte Spiel automatisch. Das Menü wird eingeschränkt und dieser Assistent ist nicht mehr zugänglich, bis die Datei .lock im Clientverzeichnis gelöscht wird.",
            type: "checkbox",
            id: "lock",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Display ist horizontal platziert, Probanden auf verschiedenen Seiten des Tisches.",
            type: "checkbox",
            id: "horizontal",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Adresse des Orchestrators",
            type: "text",
            id: "remoteUrl",
            placeholder: "https://localhost:3000",
            tooltip: "Die Adresse, von der die Sequenz geladen wird"
        },
        {
            name: "Name des Clients",
            type: "text",
            id: "clientID",
            placeholder: "Multitouchtisch Raum 6309",
            tooltip: "Ein eindeutiger Identifier"
        },
        {
            id: "mode",
            value: 3,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage_prof_science)
}
//done
let mode_supplementingClient = {
    description: "Der gewählte Modus startet bindet das Gerät als Einzelplatzgerät in ein remote-gesteuertes Forschungssetup ein.",
    inputfields: [
        {
            name: "Im Vollbild-Modus starten. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Anwendung sperren. Wenn aktiviert wird das Menü eingeschränkt und dieser Assistent ist nicht mehr zugänglich, bis die Datei .lock im Clientverzeichnis gelöscht wird.",
            type: "checkbox",
            id: "lock",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Adresse des Orchestrators",
            type: "text",
            id: "remoteUrl",
            placeholder: "https://localhost:3000",
            tooltip: "Die Adresse, von der die Sequenz geladen wird"
        },
        {
            name: "Name des Clients",
            type: "text",
            id: "clientID",
            placeholder: "Client A",
            tooltip: "Ein eindeutiger Identifier"
        },
        {
            name: "Gruppe des Clients. Wird verwendet um Sequenzabschnitte zu parallelisieren",
            type: "text",
            id: "groupID",
            placeholder: "Z.B. PrePostTest",
            tooltip: "Gruppen-Identifier"
        },
        {
            id: "mode",
            value: 4,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage_prof_science_single)
}
// still todo. likely pretty similar to supplementing client, but with injection of userID, either randomly generated or given. In addition, the sequence will start automatically, no RA needed. ExperimentID has to be given
let mode_standalone_online = {
    description: "Der gewählte Modus startet aktuell das gewählte Lernspiel. Option zur Remote-Steuerung kommt.",
    inputfields: [
        {
            name: "Im Vollbild-Modus spielen. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Spiel sperren. Wenn aktiviert startet das gewählte Spiel automatisch. Das Menü wird eingeschränkt und dieser Assistent ist nicht mehr zugänglich, bis die Datei .lock im Clientverzeichnis gelöscht wird.",
            type: "checkbox",
            id: "lock",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Display ist horizontal platziert, Probanden auf verschiedenen Seiten des Tisches.",
            type: "checkbox",
            id: "horizontal",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Internetadresse",
            type: "text",
            id: "gameURL",
            placeholder: "https://lernspiele.informatik.rwth-aachen.de",
            tooltip: "Die Adresse, von der das Spiel geladen wird"
        },
        {
            name: "oder",
            type: "label"
        },
        {
            name: "Spieldatei",
            type: "file",
            id: "gamePackage",
            value: ".mtlg",
            tooltip: "Wenn Du eine Spieldatei hast, kannst du auch diese laden"
        },
        {
            name: "Sind beide gegeben hat die Spieldatei Vorrang. Wird nichts angegeben, wird der RWTH/Learntech-Spieleserver verwendet.",
            type: "label"
        },
        {
            id: "mode",
            value: 5,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage_prof_science_single_dedicated)
}
// still todo. imports sequence and packages. sanity check on sequence if all required packages are imported. Generation of uuids in each session?
let mode_standalone_offline = {
    description: "Der gewählte Modus startet aktuell das gewählte Lernspiel. Option zur Remote-Steuerung kommt.",
    inputfields: [
        {
            name: "Im Vollbild-Modus spielen. Kann mit F11 verlassen werden.",
            type: "checkbox",
            id: "fullscreen",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Spiel sperren. Wenn aktiviert startet das gewählte Spiel automatisch. Das Menü wird eingeschränkt und dieser Assistent ist nicht mehr zugänglich, bis die Datei .lock im Clientverzeichnis gelöscht wird.",
            type: "checkbox",
            id: "lock",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Display ist horizontal platziert, Probanden auf verschiedenen Seiten des Tisches.",
            type: "checkbox",
            id: "horizontal",
            tooltip: "Startet das Spiel im Vollbild-Modus. Kann mit Esc verlassen werden"
        },
        {
            name: "Internetadresse",
            type: "text",
            id: "gameURL",
            placeholder: "https://lernspiele.informatik.rwth-aachen.de",
            tooltip: "Die Adresse, von der das Spiel geladen wird"
        },
        {
            name: "oder",
            type: "label"
        },
        {
            name: "Spieldatei",
            type: "file",
            id: "gamePackage",
            value: ".mtlg",
            tooltip: "Wenn Du eine Spieldatei hast, kannst du auch diese laden"
        },
        {
            name: "Sind beide gegeben hat die Spieldatei Vorrang. Wird nichts angegeben, wird der RWTH/Learntech-Spieleserver verwendet.",
            type: "label"
        },
        {
            id: "mode",
            value: 6,
            type: "hidden"
        }
    ],
    next: evaluateForm,
    back: ()=>setThisOrThat(usage_prof_science_single_dedicated)
}
