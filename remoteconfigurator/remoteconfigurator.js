const io = require('socket.io-client')

let socket
let mr,sr;

let initialized = false


let clientInfo = {
    clientID: 'Matthias Office',
    clientType: 'MTLGClient',
    roomID: 'R6309'
}

let expInfo = {
    experimentID: 'Feedback Study',
    experimentAuthToken: '123abc!'
}


function init(mediaRecorder, sequenceRunner, socketURL){
    if(initialized) return
    mr = mediaRecorder
    sr = sequenceRunner
    socket = io(socketURL, {path:"/orchestrator/socket.io"})
    console.log(socket)
    if(global.clientConfig.hasOwnProperty('clientID')) clientInfo.clientID = global.clientConfig.clientID;
    console.log(global.clientConfig)

    socket.on("connect",()=>{
        socket.emit('registerClient',clientInfo,(response)=>{
            global['broadcastChannel'] = response
            console.log("A channelresponse has been received, it is ",response)
            sr.endStage()
        })
    })

    socket.on('ping',()=>{
        socket.emit('pong',Date.now());
    })
//todo soon to be deprecated?
    socket.emit('joinExperiment',expInfo)

    socket.on('getAvailableSources',async (type,cb)=>{
        console.log('Request for sources received')
        let res = await mr.getAvailableStreams(type)
        console.log("returning res ", res)
        cb(res)
    })

    socket.on('requestStream',(streamID,streamType)=>{
        console.log("Received Stream request")
        mr.prepareSource(streamID, streamType).then(()=>mr.startStreaming(streamID))
    })

    socket.on('nextStage',()=>{
        sr.endStage()
    })

    socket.on('loadSequence',(seq)=>{
        sr.startSequence(seq)
    })

    initialized = true
}

exports.remoteConfigurator = {init}
