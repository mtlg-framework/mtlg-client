# MTLG Client

This is a tool to integrate multi-touch learning games into research processes
It is work in progress and is not even in Beta stage.

If you want to try it anyways, go the usual way of npm install && npm start
Things you need to know about:
- Screenrecorder works
- Videorecorder might crash if no camera is there or none is selected in the menu
- Both depend on selecting a folder for the storage of the recording, didn't bother to test what happens if you don't (yet)
- Setup wizard is just the decision tree for the different modes so far, no configuration wizards implemented yet
- A "game session" can be initiated via the menu, currently on tester.html for easy feature testing
- A "preview" on the actual functionality can be found by switch the loadFile to the commented loadUrl
- The info entry in the menu opens chromium dev-tools on the window where invoked rn, will be removed for production


Todo:
- Set a default directory for recording (appdata?)


Chaotic copy-pasta from my notepad:
Menu für "Restart Config"
Strategie für Bundleimport
->Archiv importieren, auspacken irgendwo hin kopieren?

Gameframe/MTLG-Core anpassen für Zugriff auf gegebene Config

MTLG-Remote-Control-Server

Datenstruktur für MTLG-Remote-Config

Ablaufsteuerung für Module

cleanup before unload
-stop pending recordings

sanity-check on path
schreibtest auf ausgabeverzeichnis
Timestamp (date.now) or iso-time?

main process module protocol looks promising

setinterval und settimeout in preload überladen für pause?

add getwindowlist to screenrecorder

update path on change (screenrec)

prepare and start are not yet separated (videorecorder)

Literatur rausziehen