
let demoConfiguration = {
    autoStartSequence: true,       //whether the sequence is started immediatly on reception from server, todo: on offline button to start sequence
    fullscreen: false,               //should the sequence run in fullscreen
    screenCast: true,               //which screen to record. true for entire screens (all available), otherwise array of window titles or self for only sequence, false is off
    videofeeds: true,               //which video devices to record. true for all available devices, array of device ids, false is off
    recordSelection: {
        video: true,                //true records all elements of sequence. accepts object { video: ['sequenceID1'], audio: ['sequenceID'], screen: ['sequenceID']
        audio: false,
        screen: true
    },
    splitSequence: false,           //If recordSelection is set to true or lists consecutive sections, true saves separate files for each section
    finalRecordingEndpoint: 'local',//local keeps recordings local, todo: p2p tries to send via WebRTC, web/owncloud/whatever is to be implemented yet
    finalAction: 'quit'            //remain: just keeps last position open, close: closes fullscreen window, quit: quits application
}
let demoSequence = [
    {
        id: 'Google Calibration',
        type: 'url',                        //alt: file, package, localConfig (takes gamePackage (full filepath) from clientConfig, resorts to clientConfig.gameURL if empty)
        source: 'http://google.de',         //path to file in fs, name of (preimported) package, empty for localConfig  todo: list of packages provider
        injectables:{localconfig:{abc:'def',ghi:'jkl'}},                     //given object will be made available via window.mtlgClient
        endCondition: 'content-trigger',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
        endConditionParam: 'Salamipizza',          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    },
    {
        id: 'Bing Calibration',
        type: 'url',                        //alt: file or package
        source: 'http://bing.de',         //path to file, name of package, todo: list of packages provider
        injectables:{autoCorrect: true, sossen:['mayo','joppi']},                     //given object will be made available via window.mtlgClient
        endCondition: 'event-listener',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
        endConditionParam: false,          //timeout: millis, content-trigger: string, event listener: false
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    },
    {
        id: 'Cat image',
        type: 'url',                        //alt: file or package
        source: 'https://www.myself.de/Undefined/image-thumb__8934__header/katzenfotos_1.jpeg',         //path to file, name of package, todo: list of packages provider
        injectables:{'belag':'chickenuggets'},                     //given object will be made available via window.mtlgClient
        endCondition: 'timeout',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event-listener (inject after loading?)
        endConditionParam: '5000',          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    }
]

let mode1Config = {
    autoStartSequence: true,       //whether the sequence is started immediatly on reception from server, todo: on offline button to start sequence
    fullscreen: 'localConfig',               //should the sequence run in fullscreen
    screenCast: false,               //which screen to record. true for entire screens (all available), otherwise array of window titles or self for only sequence, false is off
    videofeeds: false,               //which video devices to record. true for all available devices, array of device ids, false is off
    recordSelection: {
        video: false,                //true records all elements of sequence. accepts object { video: ['sequenceID1'], audio: ['sequenceID'], screen: ['sequenceID']
        audio: false,
        screen: false
    },
    splitSequence: false,           //If recordSelection is set to true or lists consecutive sections, true saves separate files for each section
    finalRecordingEndpoint: 'local',//local keeps recordings local, todo: p2p tries to send via WebRTC, web/owncloud/whatever is to be implemented yet
    finalAction: 'remain'            //remain: just keeps last position open, close: closes fullscreen window, quit: quits application
}
let mode1Sequence = [
    {
        id: 'MTLG Game',
        type: 'file',                        //alt: file or package
        source: './sequencing/pattern.html',         //path to file, name of package, todo: list of packages provider
        injectables:{},                     //given object will be made available via window.mtlgClient
        endCondition: 'timeout',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event listener (inject after loading?)
        endConditionParam: 5000,          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    },
    {
        id: 'MTLG Game',
        type: 'file',                        //alt: file or package
        source: './sequencing/pattern2.html',         //path to file, name of package, todo: list of packages provider
        injectables:{},                     //given object will be made available via window.mtlgClient
        endCondition: 'timeout',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event listener (inject after loading?)
        endConditionParam: 5000,          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    },
    {
        id: 'MTLG Game',
        type: 'localConfig',                        //alt: file or package
        source: '',         //path to file, name of package, todo: list of packages provider
        injectables:{},                     //given object will be made available via window.mtlgClient
        endCondition: 'event-listener',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event listener (inject after loading?)
        endConditionParam: false,          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    }
]

let remoteConfig = {
    autoStartSequence: true,       //whether the sequence is started immediatly on reception from server, todo: on offline button to start sequence
    fullscreen: 'localConfig',               //should the sequence run in fullscreen
    screenCast: false,               //which screen to record. true for entire screens (all available), otherwise array of window titles or self for only sequence, false is off
    videofeeds: false,               //which video devices to record. true for all available devices, array of device ids, false is off
    recordSelection: {
        video: false,                //true records all elements of sequence. accepts object { video: ['sequenceID1'], audio: ['sequenceID'], screen: ['sequenceID']
        audio: false,
        screen: false
    },
    splitSequence: false,           //If recordSelection is set to true or lists consecutive sections, true saves separate files for each section
    finalRecordingEndpoint: 'local',//local keeps recordings local, todo: p2p tries to send via WebRTC, web/owncloud/whatever is to be implemented yet
    finalAction: 'remain'            //remain: just keeps last position open, close: closes fullscreen window, quit: quits application
}

let remoteSequence = [
    {
        id: 'Verbindung zum Server',
        type: 'msg',                        //alt: file or package
        source: 'Verbindung zum Experiment-Management-Server wird hergestellt',         //path to file, name of package, todo: list of packages provider
        injectables:{},                     //given object will be made available via window.mtlgClient
        endCondition: 'servermessage',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event listener (inject after loading?)
        endConditionParam: '',          //timeout: millis, content-trigger: string, event listener: empty string
        transition: 'immediate',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    },
    {
        id: 'Verbindung hergestellt',
        type: 'msg',                        //alt: file or package
        source: 'Die Verbindung zum Server steht. Warte auf Versuchssequenz',         //path to file, name of package, todo: list of packages provider
        injectables:{},                     //given object will be made available via window.mtlgClient
        endCondition: 'servermessage',            //timeout ,alt: servermessage (socket.io on'nextStage'), content-trigger (webcontent/dom searchable?), event listener (inject after loading?)
        endConditionParam: '',          //timeout: millis, content-trigger: string, event listener: event name
        transition: 'onSignal',            //alt: onSignal, todo: implement waiting message
        allowBackgroundTransmission:false,  //might be used in the future todo: think about handling data transfer after session (avoid closing window)
    }
]

exports.sequences = {
    demoSequence,
    demoConfiguration,
    mode1Sequence,
    mode1Config,
    remoteSequence,
    remoteConfig
}
