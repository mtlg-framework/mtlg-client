const path = require("path");
let {app,globalShortcut, ipcMain} = require('electron')
//const {videoRecorder} = require('./videorecorder/videorecorder')

let stageIterator
let seq, cfg, cCfg
let endTimeout;
let allVideoSources
let videoRecorder

let win

function init(config, options){

    console.log("Initializing SequenceRunner")
    allVideoSources=new Set();
    stageIterator = 0

    cfg = config
    win = options.win
    cCfg = options.clientConfig
    videoRecorder = options.videoRecorder
 //   videoRecorder.init(rpath,(list)=>{
 //       list.forEach((device)=>allVideoSources.add(device.id))
 //   })

    ipcMain.on("toClient", (event, args) => {
        // Send request back to renderer process
        if(args.cmd === "endStage") endStage()
        //console.log(args, "Request from Client arrived in main")

    });

    console.log("SequenceRunner initialized, config",cfg)
 //   if(config.autoStartSequence) startSequence()
}

function startSequence(sequence){
    seq = sequence
    console.log("Starting sequence")
    let fS = cfg.fullscreen
    if(cfg.fullscreen==="localConfig") fS=cCfg.fullscreen
    if(fS){
        win.setMenuBarVisibility(false)
        win.setFullScreen(true)

        const ret = globalShortcut.register('Esc', () => {
            win.setMenuBarVisibility(true)
            win.setFullScreen(false)

            console.log('Leaving Fullscreen mode')
        })

        if (!ret) {
            console.log('registration failed')
        }
    }
    globalShortcut.register('F11', () => {
        win.setMenuBarVisibility(win.isFullScreen())
        win.setFullScreen(!win.isFullScreen())

        console.log('Switching Fullscreen mode to',win.isFullScreen())
    })
    if(cfg.videofeeds!==false){
     //   if(Array.isArray(cfg.videofeeds)) videoRecorder.prepareRecording(new Set(cfg.videofeeds))
     //   else videoRecorder.prepareRecording((allVideoSources))
    }
    //todo prepare screen recording
    //todo prepare audio recording
    nextStage(0,true);
}

function nextStage(skipN = 0, first = false){

    let endingStage = stageIterator
    stageIterator++;
    stageIterator+=skipN;
    if(first) stageIterator = 0

    console.log("Trying to start stage",stageIterator)
    if(stageIterator<seq.length){
        //there is a next stage
        // check if recording && (split || !nextstage in recording) stop recording
        handleStage(seq[stageIterator])
        // if(!recording&&newstage in recording) start recording
    }
    else {
        // end possible pending recordings
        console.log('no next stage available, end reached')
        if(cfg.finalAction==='quit') app.quit()
    }
}


function endStage() {
    console.log("Stage ended:",stageIterator)
    if(seq[stageIterator].transition==='immediate') nextStage()
    //todo else notify someone, i.e. via websocket.
}

function handleStage(stage){
    let requestID=false
    global.inject = stage.injectables

    // todo handle injectables
    switch (stage.type){
        case 'url':
            win.loadURL(stage.source)
            break;
        case 'file':
            win.loadFile(stage.source)
            break
        case 'package':
            win.loadFile(path.join('games',stage.source,'index.html'))
            break
        case 'msg':
            console.log("its a message")
            global.inject.msg = stage.source
            global.inject.title = stage.id
            global.inject.horizontal = cCfg.horizontal
            win.loadFile(path.join('sequencing','showMessage.html'))
            if(stage.endCondition==='event-listener') global.inject.showButton = true;
            break;
        case 'localConfig':
            console.log("Reached sequence runner in handlestage/case localConfig. cCfg is",cCfg)
            if(cCfg.gamePackage!=="") win.loadFile(cCfg.gamePackage);
            else if(cCfg.gameURL!=="") win.loadURL(cCfg.gameURL);
            else win.loadURL(cCfg.defaultGameURL)
            break;
    }

    switch (stage.endCondition){
        case 'timeout':
            console.log("Timeout Set")
            win.webContents.once('did-finish-load',()=>{
                endTimeout= setTimeout(()=>{
                    console.log("Stage ended on timeout")
                    endStage()
                },stage.endConditionParam)
            })
            break;
        case 'content-trigger':
            win.webContents.on('found-in-page',(e,result)=>{
                if((result.matches>0)&&requestID===result.requestId) {
                    console.log(result)
                    requestID=false
                    win.webContents.stopFindInPage("clearSelection")
                    console.log("Stage ended on found keyword")
                    win.webContents.off('did-finish-load',finder)
                    endStage()
                }
            });
           // setInterval(()=>win.webContents.findInPage(stage.endConditionParam),500) //todo test & use sane amount of millis (current approach does not work for non-loading dom updates)
            function finder(){
                win.webContents.stopFindInPage("clearSelection")
                requestID = win.webContents.findInPage(stage.endConditionParam)
            }
            win.webContents.on('did-finish-load',finder)
            break;
        case 'event-listener':
            // This is intentionally no-op. endStage will be called from bridge by calling mtlgClientBridge.stageFinished()
            break;
        case 'server-message'://this is todo
            break;
    }

}

function interruptSequence() {
    clearTimeout(endTimeout)
}

exports.sequenceRunner = {
    init,
    startSequence,
    endStage,
    interruptSequence
}
