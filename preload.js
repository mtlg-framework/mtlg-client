const {
  contextBridge,
  ipcRenderer
} = require("electron");
const r = require("electron").remote

contextBridge.exposeInMainWorld(
    "mtlgClientBridge", {
      stageFinished: () => ipcRenderer.send("toClient",{cmd:"endStage"}),
      send: (data) =>  ipcRenderer.send("toClient", data),
      receive: (func) => ipcRenderer.on("fromClient", (event, ...args) => func(...args)),
      settings: require('electron').remote.getGlobal('mtlgConfig'),//data.content
      gameSettings: require('electron').remote.getGlobal('mtlgGameSettings'),
      coreConfig: require('electron').remote.getGlobal('mtlgCoreConfig'),
      deviceConfig: require('electron').remote.getGlobal('mtlgDeviceConfig'),
    }
)

try {
  contextBridge.exposeInMainWorld("mtlgClient", require('electron').remote.getGlobal('inject'));
}
catch (e) {
  console.log("inject not yet set")
}
