const remote = require('electron').remote;
const io = require('socket.io-client')

const peerConnections = {};
const config = {
    iceServers: [
        {
            "urls": "stun:stun.l.google.com:19302",
        },
        // {
        //   "urls": "turn:TURN_IP?transport=tcp",
        //   "username": "TURN_USERNAME",
        //   "credential": "TURN_CREDENTIALS"
        // }
    ]
};

let webstream

let socket
let broadcastChannel

function init(stream){
    webstream = stream
    socket = io(remote.getGlobal('socketURL'), {path:"/orchestrator/socket.io"});
    broadcastChannel = remote.getGlobal('broadcastChannel')

    socket.emit("joinBroadcastChannel",broadcastChannel,(response)=>{
        if(response==="ok"){
            //Handle that
        } else console.log("This failed so bad")//report that somewhere
    })
    socket.on("answer", (id, description) => {
        peerConnections[id].setRemoteDescription(description);
    });

    socket.on("watcher", id => {
        peerConnections[id] =  new RTCPeerConnection(config);
        stream.getTracks().forEach(track => peerConnections[id].addTrack(track, stream));
        peerConnections[id].onicecandidate = event => {
            if (event.candidate) {
                socket.emit("candidate", id, event.candidate);
            }
        };

        peerConnections[id]
            .createOffer()
            .then(sdp => peerConnections[id].setLocalDescription(sdp))
            .then(() => {
                socket.emit("offer", id, peerConnections[id].localDescription);
            });
    });

    socket.on("candidate", (id, candidate) => {
        peerConnections[id].addIceCandidate(new RTCIceCandidate(candidate));
    });

    socket.on("disconnectPeer", id => {
        peerConnections[id].close();
        delete peerConnections[id];
    });

    window.onunload = window.onbeforeunload = () => {
        socket.close();
    };
}

function startBroadcasting(){
    socket.emit("broadcaster");
}

function stopBroadcasting(){
    socket = null
}

exports.rtc = {init, startBroadcasting, stopBroadcasting}
