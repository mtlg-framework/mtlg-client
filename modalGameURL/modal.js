const {ipcMain, BrowserWindow} = require('electron');

let inputWindow = null;
let url;

//Comm with gameURL
ipcMain.on("fromGameURL", (event, args) => {
    // Send result back to renderer process
    console.log(args.msg, "Request from GameURL arrived in main")
    switch (args.msg) {
        case "getURL":
            inputWindow.webContents.send("toGameURL", {
                cmd: 'setURL',
                content: {
                    url: url
                }
            });
            break;
        case "close":
            inputWindow.hide();
            inputWindow = null;
            break;
    }
});

let initURL = function(newURL) {
    url = newURL;
    if(inputWindow == null || inputWindow.isDestroyed()) {
        inputWindow = new BrowserWindow({
            height: 100,
            width: 480,
            show: false,
            alwaysOnTop: true,
            skipTaskbar: true,
            webPreferences: {
                nodeIntegration: true,
                enableRemoteModule: true
            }
        });
        inputWindow.loadFile('modalGameURL/modal.html');
    }
    inputWindow.show();
}

exports.modalGameURL = {
    init: initURL
}
exports.initURL = initURL;
