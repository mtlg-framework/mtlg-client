const {ipcRenderer} = require('electron');

const input = document.getElementById('gameURL');
const saveBtn = document.getElementById('saveBtn');
const cancelBtn = document.getElementById('cancelBtn');

saveBtn.onclick = function () {
    ipcRenderer.send('fromGameURL', {msg:"setURL", url: input.value})
    ipcRenderer.send('fromGameURL', {msg:"close", url: input.value})
}

cancelBtn.onclick = function () {
    ipcRenderer.send('fromGameURL', {msg:"close", url: input.value})
}

ipcRenderer.on("toGameURL", function (e, data) {
    console.log('message to Game URL', data);
    switch (data.cmd) {
        case "setURL":
            input.value = data.content.url;
            break;
    }
})
ipcRenderer.send("fromGameURL", {msg:"getURL"});
