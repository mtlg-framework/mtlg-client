const {
    contextBridge,
    ipcRenderer
} = require("electron");

// Expose protected methods that allow the renderer process to use
// the ipcRenderer without exposing the entire object


contextBridge.exposeInMainWorld(
    "mtlgClient", {
        send: (data) =>  ipcRenderer.send("toClient", data)
        ,
        receive: (func) => ipcRenderer.on("fromClient", (event, ...args) => func(...args))
        ,
        settings: require('electron').remote.getGlobal('mtlgConfig'),//data.content
        gameSettings: require('electron').remote.getGlobal('mtlgGameSettings'),
        coreConfig: require('electron').remote.getGlobal('mtlgCoreConfig'),
        deviceConfig: require('electron').remote.getGlobal('mtlgDeviceConfig'),
    }
);