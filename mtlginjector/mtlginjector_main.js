const {ipcMain, BrowserWindow} = require('electron')
const Store = require('electron-store')
const path = require('path')

let mtlgWindow;
const store = new Store();

global.mtlgConfig = {'ein eimer': 'kartoffelsalat'}; //example, test
global.mtlgGameSettings = {
    default: {
        dataLogging: "off",
        playerNumber: 2
    },
    all: {}
};
global.mtlgCoreConfig = {};
global.mtlgDeviceConfig = {};

function initMTLGSession() {
    mtlgWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: false, // is default value after Electron v5
            contextIsolation: true, // protect against prototype pollution
            enableRemoteModule: true, // turn off remote
            preload: path.join(__dirname, "preload.js"), // use a preload script

        },

        // autoHideMenuBar: true,
        //   fullscreen: true
    })

    let clientConfig = undefined;
    if (!(clientConfig = store.get('config'))) {
        mtlgWindow.loadFile(path.join(__dirname, 'tester.html'))
    } else {
        if(clientConfig.useURL) {
            if(clientConfig.tmpGameURL)
                mtlgWindow.loadURL(clientConfig.tmpGameURL)
            else
                mtlgWindow.loadURL('http://lernspiele.informatik.rwth-aachen.de/games/dem1-RegEx3-Roadshow/')
        } else {
            if(clientConfig.tmpGamePath && clientConfig.tmpGamePath.length > 0)
                mtlgWindow.loadFile(clientConfig.tmpGamePath[0]);
        }
    }
}

//Comm with MTLG-Content


exports.initMTLGSession = initMTLGSession;
