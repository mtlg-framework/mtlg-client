const {ipcRenderer, desktopCapturer} = require('electron');
const {writeFile} = require('fs');
const dayjs = require('dayjs')
const path = require('path');
const fs = require("fs");

const {rtc} = require('../src/webrtcsocket')

const streams = new Map()
let savePath
let streamID=""
let prefix = ""

const videoElement = document.querySelector('video');

async function displayIDtoID(dID){
    let sources = await desktopCapturer.getSources({types:["screen"]})
    for(const s of sources) {
        if(s.display_id===dID.toString()) ipcRenderer.send("fromVideoRecorder", {msg: "convertedDisplayID", 'did':s.id})
    }

}

async function listAvailableSources(type) {
    const availableSources = []
    let sources
    switch (type) {
        case "screen":
        case "window":
            sources = await desktopCapturer.getSources({types: [type]})
            for (const source of sources) {
                availableSources.push({
                    'id':source.id,
                    'name': source.name,
                    'type': type
                })
            }
            break;
        case "video":
        case "audio":
            sources = await navigator.mediaDevices.enumerateDevices().then(sources => {
                for (const source of sources) {
                    if (source.kind === (type+'input')) availableSources.push({
                        'id': source.deviceId,
                        'name': source.label,
                        'type': type
                    })
                }
            })
            break;
    }
    ipcRenderer.send("fromVideoRecorder", {msg: "availableSources", availableSources, type})
}

async function prepareStream(id, type, prefix) {
    if(streams[id]){
        ipcRenderer.send(id, {msg: "sourcePrepared", id})
        return
    }
    let c, startTime;
    let audio = false;
    let tag='';
    switch (type) {
        case 'screen':
        case 'window':
            c = {
                audio: false,
                video: {
                    mandatory: {
                        chromeMediaSource: 'desktop',
                        chromeMediaSourceId: id,
                    }
                }
            }
            break;
        case 'audio':
            audio = true;
        case 'video':
            c = {video: {deviceId: id, audio: audio}}
            break;
    }
    const stream = await navigator.mediaDevices.getUserMedia(c)
    const mediaRecorder = new MediaRecorder(stream, {mimeType: 'video/webm; codecs=vp9'});
    const recordedChunks = []
    mediaRecorder.ondataavailable = (e => {
        recordedChunks.push(e.data);
    });
    let iterator = 1;
    for (const s in streams) {
        if(streams[s].type===type) iterator++
    }
    tag = iterator
    mediaRecorder.onstart = (() => {
        startTime = dayjs().format("YYYY-MM-DD[T]HHmmss")
    })
    mediaRecorder.onstop = (async e => {
        const blob = new Blob(recordedChunks, {type: 'video/webm; codecs=vp9'});
        const buffer = Buffer.from(await blob.arrayBuffer());
        const filePath = path.join(savePath, `${startTime}-${prefix}-${type}-${tag}.webm`);
        writeFile(filePath, buffer, () => console.log('video saved successfully!'));
        streams[id].savedFile = filePath
        streams[id].state = "finished"
    });
    streams[id] = {
        id,
        type,
        stream,
        tag,
        prefix,
        mr: mediaRecorder,
        rc: recordedChunks,
        st: startTime,
        'state':'ready'
    }
    ipcRenderer.send(id, {msg: "sourcePrepared", id})
}

function startCastingStream(id){
    if(streamID!==""&&streamID!==id) stopCastingStream()
    streamID = id
    rtc.init(streams[id].stream)
    rtc.startBroadcasting()
}

function stopCastingStream(){
    if(streams[streamID].state !== "recording"){
        streams[streamID].stream.getTracks().forEach(s => s.stop());
        delete(streams[streamID])
    }
    rtc.stopBroadcasting()
}

function stopStreamRecording(ids){
    ids.forEach(s=>{
        streams[s].mr.stop()
    })

    function checkForRecordings(){
        if(ids.some(id=>{
            return streams[id].state==="recording"
        })){
            setTimeout(checkForRecordings,100)
        } else {
            const files = ids.map(id => {
                streams[id].state = "ready"
                let file = streams[id].savedFile
                if(id!==streamID){
                    streams[id].stream.getTracks().forEach(s => s.stop());
                    delete(streams[id])
                }
                return file

            })
            ipcRenderer.send("fromVideoRecorder", {msg: "finishedRecording", files})
        }
    }
    checkForRecordings()
}

function startStreamRecording(ids){
    ids.forEach(s=>{
        streams[s].mr.start()
        streams[s].state="recording"
    })

    ipcRenderer.send("fromVideoRecorder", {msg: "startedRecording"})
    //collect filenames
}

ipcRenderer.on("toVideoRecorder", (e, data) => {
    switch (data.cmd) {
        case "setPath":
            console.log(data.content)
            fs.access(data.content.path, fs.constants.W_OK, function(err) {
                if(err){
                    console.error("can't write to given path");
                    return
                }
                savePath = data.content.path;
            });
            break;
        case "stopStreaming":
            stopCastingStream()
            break;
        case "listDevices":
            listAvailableSources(data.type)
            break;
        case "prepareSource":
            prepareStream(data.id,data.type,data.prefix)
            break;
        case "streamSource":
            console.log("Starting Stream from ",data.id)
            startCastingStream(data.id)
            break;
        case "stopRecording":
            stopStreamRecording(data.ids)
            break;
        case "convertDID":
            displayIDtoID(data.id)
            break;
        case "stopAllNow":
            let idsOfRecording = []
            for (const s in streams) {
                if(streams[s].state==="recording") idsOfRecording.push(s)
            }
            stopStreamRecording(idsOfRecording)
            break;
        case "startStreamRecording":
            startStreamRecording(data.ids)
            break;
        case "setPrefix":
            prefix = data.content
            break;
    }
});
