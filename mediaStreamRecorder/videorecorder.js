const {ipcMain, BrowserWindow} = require('electron')

let videoRecWindow;

let recordingPath;

function init(rPath) {
    recordingPath = rPath
    videoRecWindow = new BrowserWindow({
            width: 400,
            height: 500,
            webPreferences: {
                nodeIntegration: true,
                enableRemoteModule: true,
                contextIsolation: false
            },
            show: false,
        }
    )
    videoRecWindow.loadFile('mediaStreamRecorder/record.html')
    return new Promise((resolve, reject) => {
        setTimeout(reject,10000)
        videoRecWindow.webContents.on('did-finish-load', () => {
            setRecordingPath(rPath)
            resolve()
        })
    })
}

function setRecordingPath(rPath) {
    recordingPath = rPath;
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'setPath',
        content: {
            path: rPath,
        }
    });

}

function getAvailableStreams(type) {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'listDevices',
        type
    });
    return new Promise((resolve, reject) => {
        setTimeout( reject, 10000)
        ipcMain.on("fromVideoRecorder", (event, args) => {
            if (args.msg === "availableSources") {
                resolve(args.availableSources)
            }
        });
    });
}

function prepareSource(id, type, prefix="") {
    if(id==="self"){
        id = global.winId
        type = "window"
    }
    if(id==="self_screen"){
        id = global.screenId
        type = "screen"
    }
    console.log("Requested ID is",id)
    console.log("Prefix in Videorecorder",prefix)
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'prepareSource',
        id,
        type,
        prefix
    });
    return new Promise((resolve, reject) => {
        setTimeout(reject, 10000)
        ipcMain.once(id, (event, args) => {
            if ((args.msg === "sourcePrepared") && (args.id === id)) {
                resolve(id)
            }
        });
    });
}

function startStreaming(id) {
    if(id==="self"){
        id = global.winId
    }
    if(id==="self_screen"){
        id = global.screenId
    }
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'streamSource',
        id
    });
}

function setPrefix(prefix) {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'setPrefix',
        content: prefix
    });
}

function stopRecording(ids) {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'stopRecording',
        ids
    });

    return new Promise((resolve, reject) => {
        setTimeout(reject, 300000)
        ipcMain.on("fromVideoRecorder", (event, args) => {
            if (args.msg === "finishedRecording") {
                resolve(args.files)

            }
        });
    });
}

function convertDisplayId(id) {
    console.log("I intend to convert ",id)
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'convertDID',
        id
    });

    return new Promise((resolve, reject) => {
        setTimeout(reject, 300000)
        ipcMain.on("fromVideoRecorder", (event, args) => {
            if (args.msg === "convertedDisplayID") {
                resolve(args.did)
            }
        });
    });
}

function emergencyStop() {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'stopAllNow'
    });

    return new Promise((resolve, reject) => {
        setTimeout(reject, 300000)
        ipcMain.on("fromVideoRecorder", (event, args) => {
            if (args.msg === "finishedRecording") {
                resolve(args.files)

            }
        });
    });
}

function startRecording(ids) {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'startStreamRecording',
        ids
    })
}

function stopStreaming() {
    videoRecWindow.webContents.send("toVideoRecorder", {
        cmd: 'stopStreaming'
    })
}


exports.videoRecorder = {
    init,
    setPrefix,
    setRecordingPath,
    getAvailableStreams,
    prepareSource,
    startRecording,
    stopRecording,
    startStreaming,
    stopStreaming,
    convertDisplayId,
    emergencyStop
}
